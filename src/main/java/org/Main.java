/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org;

import org.entity.Student;
import org.entity.config.DBConfiguration;
import org.entity.dao.impl.StudentDaoJpaImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(
                DBConfiguration.class);
        StudentDaoJpaImpl dao = applicationContext.getBean(StudentDaoJpaImpl.class);
        Student student = new Student();
        student.setFirstName("Grey");
        student.setLastName("Joy");
        dao.insert(student);
    }
}
