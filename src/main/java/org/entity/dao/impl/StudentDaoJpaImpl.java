/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.entity.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import org.entity.Student;
import org.entity.dao.Dao;

/**
 *
 * @author greenhorn
 */
public class StudentDaoJpaImpl implements Dao<Student> {

    /**
     * @PersistenceUnit has a unitName attribute. Its value is optional;
     * however, it can be used to inject another entityManagerFactory bean
     * defined in the container.
     */
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public boolean insert(Student t) {
        EntityManager entityManager = entityManagerFactory
                .createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();

        entityManager.persist(t);

        transaction.commit();
        entityManager.close();

        return true;
    }

    @Override
    public boolean update(Student t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Student t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
