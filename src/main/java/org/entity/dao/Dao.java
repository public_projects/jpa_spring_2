/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.entity.dao;

/**
 *
 * @author greenhorn
 */
public interface Dao<T> {

    public boolean insert(T t);

    public boolean update(T t);

    public boolean delete(T t);
}
